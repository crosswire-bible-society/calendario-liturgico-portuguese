#!/bin/bash 
#http://www.indigoblue.eu/ics2csv/ POur la conversion du ics en csv puis déplacer la colonne de la date au début avec calc.
rm 2020-p*

sed -ri 's/([1-3]) ([A-Z][a-z][a-z] [0-9])/\1\xc2\xa0\2/g' calendrier_portuguais_nv.csv
sed -ri 's/([1-3]) ([A-Z][a-z][a-z][a-z] [0-9])/\1\xc2\xa0\2/g' calendrier_portuguais_nv.csv
sed -ri 's/([1-3]) ([A-Z][a-z][a-z][a-z][a-z] [0-9])/\1\xc2\xa0\2/g' calendrier_portuguais_nv.csv
sed -ri 's/([1-3]) ([A-Z][a-z] [0-9])/\1\xc2\xa0\2/g' calendrier_portuguais_nv.csv
 sed -r 's/([0-9][0-9])-([0-9][0-9]),([A-Za-z -()º–0-9]*),\"(.*)\"/\$\$\$\1\.\2\n<div type="entry" osisID="\1\.\2">\n<title>\3<\/title>\n<p>\4<\/p><\/div>/g' calendrier_portuguais_nv.csv >2020-pt.xml

sed -ri 's/\*  /\*\n/g' 2020-pt.xml

  sed -ri 's/\t(L 1.*): (Sal .*)\t(L 2.*)\t(Ev\t.*[0-9a-g])\*/\n<list>\n<item>\1<\/item>\n<item>\2<\/item>\n<item>\3<\/item>\n<item>\4<\/item>\n<\/list>\n/g' 2020-pt.xml

 sed -ri 's/\t(L 1.*): (Sal .*)\t(Ev\t.*[0-9a-g])\*/\n<list>\n<item>\1<\/item>\n<item>\2<\/item>\n<item>\3<\/item>\n<\/list>\n/g' 2020-pt.xml
  sed -ri 's/L 1	/L 1: /g' 2020-pt.xml
    sed -ri 's/L 2	/L 2: /g' 2020-pt.xml
      sed -ri 's/Ev	/Ev: /g' 2020-pt.xml
sed -ri 's/(^[A-Z].*)\*/<p>\1<\/p>/g' 2020-pt.xml
sed -ri 's/Branco/<div type=\"subSection1\">Branco<\/div>/g' 2020-pt.xml
    sed -ri 's/Capo Verde/Capo@@@Verde/g' 2020-pt.xml
#sed -ri 's/>Verde/><div type=\"subSection\">Verde<\/div>/g' 2020-pt.xml
    sed -ri 's/Capo@@@Verde/Capo Verde/g' 2020-pt.xml
sed -ri 's/Roxo/<div type=\"subSection2\">Roxo<\/div>/g' 2020-pt.xml
sed -ri 's/Vermelho/<div type=\"subSection3\">Vermelho<\/div>/g' 2020-pt.xml
  sed -ri 's/(Sal )([0-9]* )\(([0-9]*)\), /\1\3,/g' 2020-pt.xml
 for FILE in *.xml ; do
cat debuttext.txt "$FILE" finxml.txt > /tmp/myTmpFile
mv /tmp/myTmpFile "$FILE"
#cp "$FILE" "$FILE".out
done
./Ref2Osis.sh
 rename 's/(2020-pt).xml.out/\1/g' *.out
 
mkdir -p sword/mods.d sword/modules/lexdict/zld/devotionals/perpetport/
for FILE in 2020-pt ; do
imp2ld $FILE -o sword/modules/lexdict/zld/devotionals/perpetport/$FILE -z
done
cp sword/modules/lexdict/zld/devotionals/perpetport/* ~/.sword/modules/lexdict/zld/devotionals/perpetport/
